local_configuration = {
    'VINA' : 1.7,
    'VIND' : 1.7,
    'VINA_cur_lim' : 0.7,
    'VIND_cur_lim' : 0.7,
    
#    'sensor_bias' : -50
    }


import time

from periphery import BDAQ53Periphery
periphery = BDAQ53Periphery()

#periphery.power_off_sensor_bias()
#time.sleep(1)

#periphery.power_off_SCC()
#time.sleep(1)

periphery.power_off_BDAQ()      
time.sleep(120)
        
periphery.power_on_BDAQ()
time.sleep(3)

#periphery.power_on_SCC(**local_configuration)
#time.sleep(2)

#periphery.power_on_sensor_bias(**local_configuration)
#time.sleep(2)

#print ('Bias at %iV drawing %1.3eA' % periphery.get_sensor_bias())

