#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This scan identifies stuck pixels. Stuck pixels have such a low
    threshold or are so noisy that the output of the comparator is
    effectively always high.
    It is important to identify these pixels and count them as "noisy"
    during chip qualification and chip tuning.
    Identifying stuck pixels is not completely straight forward
    since they do not change the output of the comparator and thus show no
    hits; the same result that not noisy pixels have.
'''

from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis
from bdaq53.analysis import plotting

from tqdm import tqdm

local_configuration = {
    # Warning: injecting into all pixels at once leads to n_injections-1 hits in a few pixels
    # To catch all stuck pixels inject at least 2 times (n_injections=2)
    'n_injections': 10,
    'maskfile': 'auto',
    'mask_diff': False
}


class StuckPixelScan(ScanBase):
    scan_id = "stuck_pixel_scan"

    def configure(self, **_):
        self.chip.write_cal(cal_edge_mode=0, cal_edge_width=1, cal_edge_dly=0, write=True)   # pixel CalEdge => 1
        self.chip.injection_mask[:] = False  # Enable testing for all pixels, inverted logic here!
        self.chip.enable_mask[:] = True  # Enable testing for all pixels, inverted logic here!
        self.chip.write_masks()

    def scan(self, n_injections, **_):
        '''
        Stuck pixel scan main loop

        n_injections : int
            Number of injections.
        '''

        with self.readout():
            for _ in tqdm(range(n_injections)):
                self.chip.toggle_output_select(repetitions=1)

    def analyze(self, create_pdf=True):
        with analysis.Analysis(raw_data_file=self.output_filename + '.h5') as a:
            a.analyze_data()

        if create_pdf:
            with plotting.Plotting(analyzed_data_file=a.analyzed_data_file) as p:
                p.create_standard_plots()
                p._plot_occupancy(p.HistOcc[:, :, 0].T > 0, title='Stuck pixels', z_label='# of hits', z_min=0, z_max=1, show_sum=False)


if __name__ == "__main__":
    scan = StuckPixelScan()
    scan.start(**local_configuration)
    scan.close()
