#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Source scan with RD53A. This is equivalent to external trigger scan with HitOr (BDAQ self-trigger).
'''

import zlib
import yaml
import tables as tb
import time
import numpy as np

from bdaq53.scans.scan_ext_trigger import ExtTriggerScan
from bdaq53.analysis import analysis
from bdaq53.analysis import plotting


#chip_configuration = 'default_chip.yaml'
chip_configuration = '0x0B58.yaml'

local_configuration = {
#    # Hardware settings
#    'VINA': 1.5,
#    'VIND': 1.5,
#    'VINA_cur_lim': 0.7,
#    'VIND_cur_lim': 0.7,

    # Scan parameters
    'start_column': 0,  # start column for mask
    'stop_column': 120,  # stop column for mask
    'start_row': 0,  # start row for mask
    'stop_row': 192,  # stop row for mask
    'maskfile': 'auto',
    'scan_timeout': False,  # timeout for scan after which the scan will be stopped, in seconds; if False no limit on scan time
    'max_triggers': 100000,  # number of maximum received triggers after stopping readout, if False no limit on received trigger

    'trigger_latency': 100,  # latency of trigger in units of 25 ns (BCs)
    'trigger_delay': 65,  # trigger delay in units of 25 ns (BCs)
    'trigger_length': 32,  # length of trigger command (amount of consecutive BCs are read out)
    'veto_length': 500,  # length of TLU veto in units of 25 ns (BCs). This vetos new triggers while not all data is revieved. Should be adjusted for longer trigger length.

    # Trigger configuration
    'TRIGGER': {
        'TRIGGER_MODE': 0,  # Selecting trigger mode: Use trigger inputs/trigger select (0), TLU no handshake (1), TLU simple handshake (2), TLU data handshake (3)
        'TRIGGER_LOW_TIMEOUT': 0,  # Maximum wait cycles for TLU trigger low.
        'TRIGGER_SELECT': 1,  # Selecting trigger input: HitOR (1), disabled (0)
        'TRIGGER_INVERT': 0,  # Inverting trigger input: HitOR (1), disabled (0)
        'TRIGGER_VETO_SELECT': 0,  # Selecting trigger veto: RX1 (2), RX FIFO full (1), disabled (0)
        'TRIGGER_HANDSHAKE_ACCEPT_WAIT_CYCLES': 5,  # TLU trigger minimum length in TLU clock cycles
        'DATA_FORMAT': 0,  # Select trigger data format: only trigger number (0), only time stamp (1), combined, 15 bit time stamp + 16 bit trigger number (2)
        'EN_TLU_VETO': 0,  # Assert TLU veto when external veto
        'TRIGGER_COUNTER': 0
    }
}


class SourceScan(ExtTriggerScan):
    scan_id = "source_scan"

    def configure(self, **kwargs):
        super(SourceScan, self).configure(load_hitbus_mask=True, **kwargs)

#        self.chip.write_register(register='GP_LVDS_ROUTE', data=0x0005, write=True)
#        self.chip.write_register(register='GP_LVDS_ROUTE', data=0x0002, write=True)
        self.chip.write_register(register='GP_LVDS_ROUTE', data=0x0000, write=True)

#        self.chip.enable_core_col_clock(core_cols=range(20,50), write=True)
        # # configure self-trigger
        # self.chip['pulser_trig'].reset()
        # self.chip['pulser_trig'].set_en(False)
        # self.chip['pulser_trig'].set_delay(10000)
        # self.chip['pulser_trig'].set_width(32 * 4)
        # self.chip['pulser_trig'].set_repeat(0)
        # self.chip['pulser_trig'].start()

        # self.chip['pulser_veto'].set_repeat(0)
        # self.chip['pulser_veto'].start()

        # self.chip['cmd'].set_ext_start(True)
        # self.chip['cmd'].set_ext_trigger(True)

        # # configure az pulse generator
        # self.chip['pulse_gen_az'].set_delay(100)
        # self.chip['pulse_gen_az'].set_repeat(0)
        # self.chip['pulse_gen_az'].start()

        #mask_data = self.prepare_injection_masks(start_column, stop_column, start_row, stop_row, mask_step)


        from pylab import imread
        img = np.invert(imread('../logo.bmp').astype('bool'))
        img_mask = np.zeros((400,192), dtype=bool)

        img_mask[55:245, 1:191] = np.transpose(img[:, :, 0])
        img_mask[:,0] = False
        img_mask[:,-1] = False
        img_mask[0:55,:] = False
        img_mask[245:400,:] = False

        self.chip.hitbus_mask = img_mask
        #self.chip.hitbus_mask[:,:] = True
        print self.chip.hitbus_mask[:,100]

        self.chip.enable_mask[120:128,:] = False
        self.chip.write_masks()


        print 'Enabling the auto-zeroing'
        self.chip.az_setup(delay=80, repeat=0, width=6, synch=1)
        self.chip.az_start()

#        time.sleep(5)
#        self.chip.az_stop()

        #self.chip.send_trigger(trigger=0b1111, write=False)*8

        #indata = self.write_sync_01(write=False)*10
        '''
        if send_ecr:
            indata += self.write_global_pulse(width=8, write=False)
            indata += self.write_ecr(write=False)
            indata += self.write_sync_01(write=False)*20

        indata += self.write_cal(cal_edge_mode=0, cal_edge_width=1, cal_edge_dly=0, write=False)        # CalEdge -> 1 (inject)
        indata += self.write_sync_01(write=False)*latency                                               # Wait for latency
        indata += self.send_trigger(trigger=0b1111, write=False)*8                                      # Trigger
        indata += self.write_cal(cal_edge_mode=1, cal_edge_width=0, cal_edge_dly=0, write=False)        # CalEdge -> 0

        if self.hw_map[self.board_version] != 'SIMULATION':
            for _ in range(wait_cycles):
                indata += self.write_sync_01(write=False)*40                                                # Wait for data

        if write:
            self.write_command(indata, repetitions=100)

        return indata
        '''


    def analyze(self, create_pdf=True):

        # phi_AZ
        self.chip.az_stop()

        # # reset pulse generators
        self.chip['pulse_gen_az'].reset()

        with analysis.Analysis(raw_data_file=self.output_filename + '.h5', cluster_hits=True) as a:
            a.analyze_data()

            with tb.open_file(a.analyzed_data_file) as in_file:
                clusters = in_file.root.Cluster[:]
                n_clusters = clusters.shape[0]

        if create_pdf:
            with plotting.Plotting(analyzed_data_file=a.analyzed_data_file) as p:
                p.create_standard_plots()

        return n_clusters


if __name__ == "__main__":
    scan = SourceScan()
    scan.start(**local_configuration)
    print scan.analyze()

