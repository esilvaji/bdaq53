import yaml
import logging
import os
import time
import struct
import numpy as np
import tables as tb

from tables.exceptions import NoSuchNodeError


from basil.utils.BitLogic import BitLogic

from bdaq53.rd53a import RD53A
from basil.dut import Dut

loglevel = logging.INFO
''' Set up main logger '''


logging.SUCCESS = 25  # WARNING(30) > SUCCESS(25) > INFO(20)
logging.addLevelName(logging.SUCCESS, 'SUCCESS')


logger = logging.getLogger('RD53A')
logger.setLevel(loglevel)
logger.success = lambda msg, *args, **kwargs: logger.log(logging.SUCCESS, msg, *args, **kwargs)


def _measure_temperature_ntc_CERNFMC(self, fmc_hpc=False ):
     
        #-----constants-------------
        Vdd=2.5 #Vdd of the ADC
        R1=39000 #Resistance 1 of the voltage divider, in series with NTC in FMC-card
        R25C=10e3 #NTC constant
        T25=298.15
        B=3435 # Beta NTC constant
        LSB=0.001 # LSB value of the ADC for notmal configuration. Can be changed by changing the configuration register.
        #---------------------------
       
        #Code needed to read the values of the 1 bit ADC in CERN-FMC card:
        if (fmc_hpc):
                                #raw_input("press to write HIGH pin count connector FMC")
            self['i2c'].write(0xe8,[0b00000010])#address of switch in the FPGA, it selects several I2C devices, we select output connected to FMC HPC
                                #print "USING HPC FMC"
        else:
                                #raw_input("press to write low pin count connector FMC")
            self['i2c'].write(0xe8,[0b00000110])#address of switch in the FPGA, it selects several I2C devices, we select output connected to FMC HPC
                                #print "USING LPC FMC"
 
        self['i2c'].write(0x90,[0b00000001])#address of ADC and write the addresss pointer register to point the configuration register(default 0x8583)
        self['i2c'].write(0x90,[0b00000001,0x85,0x83])#Here we reset the ADC to start a single conversion. with this config (which is the default) in the conversion register we will read the voltage drop between the terminals of the NTC resistor.
       
        
        self['i2c'].write(0x90,[0b00000000])
        a = self['i2c'].read(0x90,2) #read two bytes of the conversion register of adc
       
        #print a, hex(a[0]), hex(a[1])
        #a[0] is the more significant byte
        #a[1] is the less significant byte, but its 4 LSBs are not part of the conversion, just zeros (it's a 12bit ADC)
        adc=(((a[0]<<8)|a[1])>>4)
        Vadc=LSB*adc
        Rntc = (R1*Vadc)/(Vdd-Vadc)
        Tntc = (1.0/((1.0/T25)+((1.0/B)*(np.log(Rntc/R25C)))))-273.15
        return Tntc


if __name__ == '__main__': 
    rd53a_chip = RD53A()
    rd53a_chip.init()
    T= rd53a_chip._measure_temperature_ntc_CERNFMC()
    logger.info("Temperature measure with NTC is: %f C", T)
    
