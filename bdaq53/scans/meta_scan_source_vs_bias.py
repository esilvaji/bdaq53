#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#


'''
    This meta script performs a simple source scan at different bias voltages.
'''

import zlib  # Workaround

import tables as tb

from bdaq53.meta_scan_base import MetaScanBase
from bdaq53.scans.scan_source import SourceScan
from bdaq53.analysis.plotting import Plotting


local_configuration = {
    # Bias voltage paramters
    'VBIAS_start': 0,
    'VBIAS_stop': -10,
    'VBIAS_step': -5,

    # Scan parameters
    'start_column': 128,  # start column for mask
    'stop_column': 264,  # stop column for mask
    'start_row': 0,  # start row for mask
    'stop_row': 192,  # stop row for mask
    'maskfile': 'auto',
    'scan_timeout': 30,  # timeout for scan after which the scan will be stopped, in seconds; if False no limit on scan time
    'max_triggers': False,  # number of maximum received triggers after stopping readout, if False no limit on received trigger

    'trigger_latency': 100,  # latency of trigger in units of 25 ns (BCs)
    'trigger_delay': 65,  # trigger delay in units of 25 ns (BCs)
    'trigger_length': 32,  # length of trigger command (amount of consecutive BCs are read out)
    'veto_length': 500,  # length of TLU veto in units of 25 ns (BCs). This vetos new triggers while not all data is revieved. Should be adjusted for longer trigger length.

    # Trigger configuration
    'TRIGGER': {
        'TRIGGER_MODE': 0,  # Selecting trigger mode: Use trigger inputs/trigger select (0), TLU no handshake (1), TLU simple handshake (2), TLU data handshake (3)
                'TRIGGER_LOW_TIMEOUT': 0,  # Maximum wait cycles for TLU trigger low.
                'TRIGGER_SELECT': 1,  # Selecting trigger input: HitOR (1), disabled (0)
                'TRIGGER_INVERT': 0,  # Inverting trigger input: HitOR (1), disabled (0)
                'TRIGGER_VETO_SELECT': 0,  # Selecting trigger veto: RX1 (2), RX FIFO full (1), disabled (0)
                'TRIGGER_HANDSHAKE_ACCEPT_WAIT_CYCLES': 5,  # TLU trigger minimum length in TLU clock cycles
                'DATA_FORMAT': 0,  # Select trigger data format: only trigger number (0), only time stamp (1), combined, 15 bit time stamp + 16 bit trigger number (2)
                'EN_TLU_VETO': 0,  # Assert TLU veto when external veto
                'TRIGGER_DATA_DELAY': 7,  # Depends on the cable length and should be adjusted (run scan/tune_tlu.py in bdaq53 repository)
                'TRIGGER_COUNTER': 0,
    }
}


class RawDataTable(tb.IsDescription):
    bias_voltage = tb.Int32Col(pos=1)
    n_clusters = tb.Float64Col(pos=2)


class MetaBiasScan(MetaScanBase):
    scan_id = 'meta_bias_source_scan'

    def start(self, **kwargs):
        filename = self.output_filename + '.h5'
        self.h5_file = tb.open_file(filename, mode='w', title=self.scan_id)
        self.raw_data_table = self.h5_file.create_table(self.h5_file.root, name='iv_data', title='IV data', description=RawDataTable)

        self.dump_configuration(**kwargs)

        self.scan(**kwargs)
        self.clean_up()

        self.h5_file.close()

    def scan(self, VBIAS_start=0, VBIAS_stop=-5, VBIAS_step=-1, **kwargs):
        add = 1 if VBIAS_stop > 0 else -1

        for bias in range(VBIAS_start, VBIAS_stop + add, VBIAS_step):
            scn = SourceScan(record_chip_status=False)

            self.logger.info('Running source scan at V_bias = %1.0fV' % bias)
            scn.periphery.set_sensor_bias(bias)

            self.logger.info('Bias current before scan: %e' % float(scn.periphery.get_sensor_bias()[1]))

            scn.logger.addHandler(self.fh)
            scn.start(**kwargs)
            n_clusters = scn.analyze(create_pdf=False)

            row = self.raw_data_table.row
            row['bias_voltage'] = bias
            row['n_clusters'] = n_clusters
            row.append()
            self.raw_data_table.flush()

            scn.close()

            self.scans.append(scn)

            self.logger.info('Bias current after scan: %e' % float(scn.periphery.get_sensor_bias()[1]))

    def analyze(self):
        with tb.open_file(self.output_filename + '.h5') as in_file:
            with tb.open_file(self.output_filename + '_interpreted.h5', 'a') as outfile:
                outfile.copy_node(in_file.root.iv_data, outfile.root, recursive=True)

        with Plotting(analyzed_data_file=self.output_filename + '.h5') as p:
            p.create_standard_plots()


if __name__ == '__main__':
    bias_scan = MetaBiasScan()
    bias_scan.start(**local_configuration)
