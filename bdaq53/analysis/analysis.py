#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Script to convert raw data
'''

from __future__ import division

import os.path

import numpy as np
import logging
import yaml

import tables as tb
from tqdm import tqdm
from scipy.optimize import curve_fit
from pixel_clusterizer.clusterizer import HitClusterizer

from bdaq53.scan_base import DacTable, RunConfigTable
from bdaq53.analysis import analysis_utils as au


loglevel = logging.getLogger('RD53A').getEffectiveLevel()

np.warnings.filterwarnings('ignore')


class Analysis(object):
    """
        Class to analyze RD53A raw data
    """

    def __init__(self, raw_data_file=None, analyzed_data_file=None,
                 store_hits=False, cluster_hits=False, align_method=0, chunk_size=1000000):
        '''
            Parameters
            ----------
            raw_data_file : string
                A string raw data file name. File ending (.h5).
            analyzed_data_file : string
                The file name of the output analyzed data file.
                File ending (.h5) does not have to be set.
            create_pdf : boolean
                Creates interpretation plots into one PDF file. Only active if
                raw_data_file is given.
            level : string
                The level of results. E.g. 'Preliminary'
            cluster_hits : boolean
                Create cluster table, histograms and plots
            align_method : integer
                Methods to do event alignment
                0: New event when number if event headers exceeds number of
                   sub-triggers. Many fallbacks for corrupt data implemented.
                1: New event when data word is TLU trigger word, with error checks
                2: Force new event always at TLU trigger word, no error checks
        '''
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.setLevel(loglevel)

        for h in logging.getLogger('RD53A').handlers:
            if isinstance(h, logging.FileHandler):
                self.logger.addHandler(h)

        self.raw_data_file = raw_data_file
        self.analyzed_data_file = analyzed_data_file
        self.store_hits = store_hits
        self.cluster_hits = cluster_hits
        self.chunk_size = chunk_size
        self.align_method = align_method
        self.compatibility = False

        if not os.path.isfile(raw_data_file):
            raise IOError('Raw data file %s does not exist.', raw_data_file)

        if not self.analyzed_data_file:
            self.analyzed_data_file = raw_data_file[:-3] + '_interpreted.h5'

        # Global variables to store info between chunks
        self.event_number = 0
        self.chunk_offset = 0  # Remaining raw data words after chunk analysis
        self.trg_id = -1
        self.prev_trg_number = -1
        self.last_chunk = False

        self._setup_clusterizer()
        self._get_run_config()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        if exc_type is not None:
            self.logger.exception("Exception during in analysis",
                                  exc_info=(exc_type, exc_value, traceback))

        for h in self.logger.handlers:
            if isinstance(h, logging.FileHandler):
                self.logger.removeHandler(h)

    def _get_run_config(self):
        ''' Load run config to allow analysis routines to access these info '''
        with tb.open_file(self.raw_data_file, 'r') as in_file:
            self.run_config = au.ConfigDict(in_file.root.configuration.run_config[:])

    def _setup_clusterizer(self):
        ''' Define data structure and settings for hit clusterizer package '''
        # Define all field names and data types
        hit_fields = {'event_number': 'event_number',
                      'ext_trg_number': 'ext_trg_number',
                      'trigger_id': 'trigger_id',
                      'bcid': 'bcid',
                      'rel_bcid': 'frame',
                      'col': 'column',
                      'row': 'row',
                      'tot': 'charge',
                      'scan_param_id': 'scan_param_id',
                      'trigger_tag': 'trigger_tag',
                      'event_status': 'event_status'
                      }
        hit_dtype = np.dtype([('event_number', '<i8'),
                              ('ext_trg_number', 'u4'),
                              ('trigger_id', 'u1'),
                              ('bcid', '<u2'),
                              ('rel_bcid', 'u1'),
                              ('col', '<u2'),
                              ('row', '<u2'),
                              ('tot', 'u1'),
                              ('scan_param_id', 'u4'),
                              ('trigger_tag', 'u1'),
                              ('event_status', 'u4')])
        cluster_fields = {'event_number': 'event_number',
                          'column': 'column',
                          'row': 'row',
                          'size': 'n_hits',
                          'id': 'ID',
                          'tot': 'charge',
                          'scan_param_id': 'scan_param_id',
                          'seed_col': 'seed_column',
                          'seed_row': 'seed_row',
                          'mean_col': 'mean_column',
                          'mean_row': 'mean_row'}
        self.cluster_dtype = np.dtype([('event_number', '<i8'),
                                       ('id', '<u2'),
                                       ('size', '<u2'),
                                       ('tot', '<u2'),
                                       ('seed_col', '<u1'),
                                       ('seed_row', '<u2'),
                                       ('mean_col', '<f4'),
                                       ('mean_row', '<f4'),
                                       ('dist_col', '<u4'),
                                       ('dist_row', '<u4'),
                                       ('cluster_shape', '<i8'),
                                       ('scan_param_id', 'u4')])

        if self.cluster_hits:  # Allow analysis without clusterizer installed
            # Define end of cluster function to calculate cluster shape
            # and cluster distance in column and row direction
            def end_of_cluster_function(hits, clusters, cluster_size,
                                        cluster_hit_indices, cluster_index,
                                        cluster_id, charge_correction,
                                        noisy_pixels, disabled_pixels,
                                        seed_hit_index):
                hit_arr = np.zeros((15, 15), dtype=np.bool_)
                center_col = hits[cluster_hit_indices[0]].column
                center_row = hits[cluster_hit_indices[0]].row
                hit_arr[7, 7] = 1
                min_col = hits[cluster_hit_indices[0]].column
                max_col = hits[cluster_hit_indices[0]].column
                min_row = hits[cluster_hit_indices[0]].row
                max_row = hits[cluster_hit_indices[0]].row
                for i in cluster_hit_indices[1:]:
                    if i < 0:  # Not used indeces = -1
                        break
                    diff_col = np.int32(hits[i].column - center_col)
                    diff_row = np.int32(hits[i].row - center_row)
                    if np.abs(diff_col) < 8 and np.abs(diff_row) < 8:
                        hit_arr[7 + hits[i].column - center_col,
                                7 + hits[i].row - center_row] = 1
                    if hits[i].column < min_col:
                        min_col = hits[i].column
                    if hits[i].column > max_col:
                        max_col = hits[i].column
                    if hits[i].row < min_row:
                        min_row = hits[i].row
                    if hits[i].row > max_row:
                        max_row = hits[i].row

                if max_col - min_col < 8 and max_row - min_row < 8:
                    # Make 8x8 array
                    col_base = 7 + min_col - center_col
                    row_base = 7 + min_row - center_row
                    cluster_arr = hit_arr[col_base:col_base + 8,
                                          row_base:row_base + 8]
                    # Finally calculate cluster shape
                    # uint64 desired, but numexpr and others limited to int64
                    if cluster_arr[7, 7] == 1:
                        cluster_shape = np.int64(-1)
                    else:
                        cluster_shape = np.int64(
                            au.calc_cluster_shape(cluster_arr))
                else:
                    # Cluster is exceeding 8x8 array
                    cluster_shape = np.int64(-1)

                clusters[cluster_index].cluster_shape = cluster_shape
                clusters[cluster_index].dist_col = max_col - min_col + 1
                clusters[cluster_index].dist_row = max_row - min_row + 1

            # Initialize clusterizer with custom hit/cluster fields
            self.clz = HitClusterizer(
                hit_fields=hit_fields,
                hit_dtype=hit_dtype,
                cluster_fields=cluster_fields,
                cluster_dtype=self.cluster_dtype,
                min_hit_charge=0,
                max_hit_charge=13,
                column_cluster_distance=3,
                row_cluster_distance=3,
                frame_cluster_distance=2,
                ignore_same_hits=True)

            # Set end_of_cluster function for shape and distance calculation
            self.clz.set_end_of_cluster_function(end_of_cluster_function)

    def _range_of_parameter(self, meta_data):
        ''' Calculate the raw data word indeces of each scan parameter
        '''
        _, index = np.unique(meta_data['scan_param_id'], return_index=True)
        expected_values = np.arange(np.max(meta_data['scan_param_id']) + 1)

        # Check for scan parameter IDs with no data
        sel = np.isin(expected_values, meta_data['scan_param_id'])
        if not np.all(sel):
            self.logger.warning('No words for scan parameter IDs: %s', str(expected_values[~sel]))

        start = meta_data[index]['index_start']
        stop = np.append(start[:-1] + np.diff(start), meta_data[-1]['index_stop'])

        return np.column_stack((expected_values[sel], start, stop))

    def _words_of_parameter(self, par_range, data):
        ''' Yield all raw data words of a scan parameter

            Do not exceed chunk_size. Use a global offset
            parameter.
        '''

        for scan_par_id, start, stop in par_range:
            for i in range(start, stop, self.chunk_size):
                # Shift chunk index to not split events. The offset is determined from previous analyzed chunk.
                # Restrict maximum offset, can happen for readouts without a single event, issue #171
                chunk_offset = max(self.chunk_offset, -self.chunk_size)
                start_chunk = i + chunk_offset

                # Limit maximum read words by chunk size
                stop_limited = min(i + self.chunk_size, stop)

                yield scan_par_id, data[start_chunk:stop_limited]

        # Remaining data of last chunk
        self.last_chunk = True  # Set flag for special treatmend
        if self.chunk_offset == 0:
            return
        yield scan_par_id, data[stop + self.chunk_offset:stop]

    def _get_attributes_from_meta_data(self, in_file, out_file):
        run_config_table = out_file.create_table(out_file.root.configuration, name='run_config', title='Run config', description=RunConfigTable)
        row = run_config_table.row
        row['attribute'] = 'scan_id'
        row['value'] = in_file.root.meta_data.attrs.scan_id
        row.append()
        row = run_config_table.row
        row['attribute'] = 'run_name'
        row['value'] = in_file.root.meta_data.attrs.run_name
        row.append()
        row = run_config_table.row
        row['attribute'] = 'software_version'
        row['value'] = in_file.root.meta_data.attrs.software_version
        row.append()
        row = run_config_table.row
        row['attribute'] = 'chip_id'
        row['value'] = in_file.root.meta_data.attrs.chip_id
        row.append()

        for kw, value in yaml.load(in_file.root.meta_data.attrs.kwargs).iteritems():
            if kw not in ['start_column', 'stop_column', 'start_row', 'stop_row', 'mask_step', 'maskfile', 'disable', 'n_injections', 'n_triggers', 'limit', 'VCAL_MED', 'VCAL_HIGH', 'VCAL_HIGH_start', 'VCAL_HIGH_stop', 'VCAL_HIGH_step', 'VTH_start', 'VTH_stop', 'VTH_step', 'VTH_name', 'vth_offset', 'DAC', 'type', 'value_start', 'value_stop', 'value_step', 'addresses']:
                continue
            row = run_config_table.row
            row['attribute'] = kw
            row['value'] = value if isinstance(value, str) else str(value)
            row.append()
        run_config_table.flush()
        run_config = dict(run_config_table[:])

        dac_table = out_file.create_table(out_file.root.configuration, name='dacs', title='DACs', description=DacTable)
        for dac, value in yaml.load(in_file.root.meta_data.attrs.dacs).iteritems():
            row = dac_table.row
            row['DAC'] = dac
            row['value'] = value
            row.append()
        dac_table.flush()

        tdac_mask = np.zeros((400, 192), dtype=int)
        if 'maskfile' in run_config.keys():
            try:
                with tb.open_file(run_config['maskfile'], 'r') as infile:
                    tdac_mask = infile.root.disable_mask[:]
            except (tb.exceptions.NoSuchNodeError, IOError):
                pass

        out_file.create_carray(out_file.root.configuration,
                               name='TDAC_mask',
                               title='TDAC mask',
                               obj=tdac_mask,
                               filters=tb.Filters(complib='blosc',
                                                          complevel=5,
                                                          fletcher32=False))

        enable_mask = self.mask_disabled_pixels(np.zeros((400, 192), dtype=bool), run_config)
        out_file.create_carray(out_file.root.configuration,
                               name='enable_mask',
                               title='Enable mask',
                               obj=enable_mask,
                               filters=tb.Filters(complib='blosc',
                                                          complevel=5,
                                                          fletcher32=False))

    def calculate_inl_dnl(self, data):
        LSB = (data[-1][1] - data[0][1]) / (data[-1][0] - data[0][0])
        x = [i[0] for i in data]
        y = [i[1] for i in data]
        results = np.zeros((len(data) - 1), dtype={'names': ['dac', 'voltage', 'dnl', 'inl'],
                                                   'formats': ['uint16', 'float64', 'float64', 'float64']})

        def _lin(x, *p):
            m, b = p
            return m * x + b

        try:
            p0 = (1., 0.)
            coeff, _ = curve_fit(_lin, x, y, p0=p0)
        except RuntimeError:
            coeff = (0, 0)
            self.logger.error('Linear fit failed!')

        for i in range(len(data) - 1):
            results['dac'][i] = data[i][0]
            results['voltage'][i] = data[i][1]
            results['dnl'][i] = (data[i + 1][1] - data[i][1]) / LSB - 1
            results['inl'][i] = (data[i][1] - _lin(data[i][0], *coeff)) / coeff[0]

        return results

    def analyze_adc_data(self):
        self.logger.info('Analyzing register data...')

        self.chunk_offset = 0

        with tb.open_file(self.raw_data_file) as in_file:
            meta_data = in_file.root.meta_data[:]
            dac_data = in_file.root.dac_data[:]
            run_config = au.ConfigDict(in_file.root.configuration.run_config[:])

            dac_dict = dict(np.column_stack((dac_data['scan_param_id'], dac_data['dac'])))
            voltage_dict = dict(np.column_stack((dac_data['scan_param_id'], dac_data['voltage'])))

            if meta_data.shape[0] == 0:
                self.logger.warning('Data is empty. Skip analysis!')
                return

            par_range = self._range_of_parameter(meta_data)
            userk_out = np.zeros(par_range.shape[0], dtype={'names': ['dac', 'adc', 'voltage'],
                                                            'formats': ['uint16', 'uint16', 'float32']})

            pbar = tqdm(total=np.max(meta_data['scan_param_id']) + 1, unit=' Words')
            i = 0
            for scan_param_id, words in self._words_of_parameter(par_range, in_file.root.raw_data):
                userk_data = au.interpret_userk_data(words)
                userk_out[i]['adc'] = au.process_userk(userk_data)[-1]['Data']
                userk_out[i]['dac'] = dac_dict[scan_param_id]
                userk_out[i]['voltage'] = voltage_dict[scan_param_id]
                i += 1
                pbar.update(1)
            pbar.close()

            with tb.open_file(self.analyzed_data_file, 'w', title=in_file.title) as out_file:
                out_file.create_table(out_file.root, 'Adc', userk_out,
                                      title='Adc',
                                      filters=tb.Filters(complib='blosc',
                                                         complevel=5,
                                                         fletcher32=False))

                if run_config['value_step'] == 1:
                    multi_data = self.calculate_inl_dnl(np.column_stack((userk_out['dac'], userk_out['voltage'])))
                    out_file.create_table(out_file.root, 'non_linearity', multi_data,
                                          title='non_linearity',
                                          filters=tb.Filters(complib='blosc',
                                                             complevel=5,
                                                             fletcher32=False))

                try:
                    out_file.create_group(out_file.root, name='configuration', title='Configuration')
                    out_file.copy_children(in_file.root.configuration, out_file.root.configuration, recursive=True)
                except tb.exceptions.NoSuchNodeError:
                    self.logger.info('No configuration data found. Assuming old data format!')
                    self.compatibility = True

                if self.compatibility:
                    self._get_attributes_from_meta_data(in_file, out_file)

    def _create_hit_table(self, out_file, dtype):
        ''' Create hit table node for storage in out_file.
            Copy configuration nodes from raw data file.
        '''
        hit_table = out_file.create_table(out_file.root, name='Hits',
                                          description=dtype,
                                          title='hit_data',
                                          expectedrows=self.chunk_size,
                                          filters=tb.Filters(complib='blosc',
                                                             complevel=5,
                                                             fletcher32=False))

        return hit_table

    def analyze_data(self):
        self.logger.info('Analyzing data...')
        self.chunk_offset = 0
        with tb.open_file(self.raw_data_file) as in_file:
            n_words = in_file.root.raw_data.shape[0]
            meta_data = in_file.root.meta_data[:]

            if meta_data.shape[0] == 0:
                self.logger.warning('Data is empty. Skip analysis!')
                return

            par_range = self._range_of_parameter(meta_data)
            n_scan_params = np.max(meta_data['scan_param_id']) + 1

            (hits, hist_occ, hist_tot, hist_rel_bcid,
             hist_event_status,
             hist_bcid_error) = au.init_outs(n_hits=self.chunk_size * 4,
                                             n_scan_params=n_scan_params)

            with tb.open_file(self.analyzed_data_file, 'w', title=in_file.title) as out_file:
                try:
                    out_file.create_group(out_file.root, name='configuration', title='Configuration')
                    out_file.copy_children(in_file.root.configuration, out_file.root.configuration, recursive=True)
                except tb.exceptions.NoSuchNodeError:
                    self.logger.info('No configuration data found. Assuming old data format!')
                    self.compatibility = True

                if self.store_hits:
                    hit_table = self._create_hit_table(out_file, dtype=hits.dtype)

                if self.compatibility:
                    self._get_attributes_from_meta_data(in_file, out_file)

                if self.cluster_hits:
                    cluster_table = out_file.create_table(
                        out_file.root, name='Cluster',
                        description=self.cluster_dtype,
                        title='Cluster',
                        filters=tb.Filters(complib='blosc',
                                           complevel=5,
                                           fletcher32=False))
                    hist_cs_size = np.zeros(shape=(100, ), dtype=np.uint32)
                    hist_cs_tot = np.zeros(shape=(100, ), dtype=np.uint32)
                    hist_cs_shape = np.zeros(shape=(300, ), dtype=np.int32)

                self.last_chunk = False
                pbar = tqdm(total=n_words, unit=' Words', unit_scale=True)
                for scan_param_id, words in self._words_of_parameter(par_range, in_file.root.raw_data):
                    prev_event_number = self.event_number
                    (n_hits, self.event_number, self.chunk_offset, self.trg_id, self.prev_trg_number) = au.interpret_data(
                        rawdata=words,
                        hits=hits,
                        hist_occ=hist_occ,
                        hist_tot=hist_tot,
                        hist_rel_bcid=hist_rel_bcid,
                        hist_event_status=hist_event_status,
                        hist_bcid_error=hist_bcid_error,
                        align_method=self.align_method,
                        event_number=prev_event_number,
                        scan_param_id=scan_param_id,
                        prev_trig_id=self.trg_id,
                        prev_trg_number=self.prev_trg_number,
                        last_chunk=self.last_chunk)

                    if prev_event_number == self.event_number:
                        self.logger.warning('Not a single event found in %d raw data words. Likely corrupt data!\n Example raw data words %s', words.shape[0], str(words[:10]))

                    if self.store_hits:
                        hit_table.append(hits[:n_hits])
                        hit_table.flush()

                    if self.cluster_hits:
                        _, cluster = self.clz.cluster_hits(hits[:n_hits])
                        cluster_table.append(cluster)
                        # Create actual cluster hists
                        cs_size = np.bincount(cluster['size'],
                                              minlength=100)[:100]
                        cs_tot = np.bincount(cluster['tot'],
                                             minlength=100)[:100]
                        sel = np.logical_and(cluster['cluster_shape'] > 0,
                                             cluster['cluster_shape'] < 300)
                        cs_shape = np.bincount(cluster['cluster_shape'][sel],
                                               minlength=300)[:300]
                        # Add to total hists
                        hist_cs_size += cs_size.astype(np.uint32)
                        hist_cs_tot += cs_tot.astype(np.uint32)
                        hist_cs_shape += cs_shape.astype(np.uint32)

                    pbar.update(words.shape[0] + self.chunk_offset)
                pbar.close()

        self._create_additional_hit_data(hist_occ, hist_tot, hist_rel_bcid, hist_event_status, hist_bcid_error)
        if self.cluster_hits:
            self._create_additional_cluster_data(
                hist_cs_size, hist_cs_tot, hist_cs_shape)

        return hits, hist_occ, hist_tot, hist_rel_bcid

    def _create_additional_hit_data(self, hist_occ, hist_tot, hist_rel_bcid,
                                    hist_event_status, hist_bcid_error):
        with tb.open_file(self.analyzed_data_file, 'r+') as out_file:
            scan_id = self.run_config['scan_id']

            out_file.create_carray(out_file.root,
                                   name='HistOcc',
                                   title='Occupancy Histogram',
                                   obj=hist_occ,
                                   filters=tb.Filters(complib='blosc',
                                                      complevel=5,
                                                      fletcher32=False))
            out_file.create_carray(out_file.root,
                                   name='HistTot',
                                   title='ToT Histogram',
                                   obj=hist_tot,
                                   filters=tb.Filters(complib='blosc',
                                                      complevel=5,
                                                      fletcher32=False))
            out_file.create_carray(out_file.root,
                                   name='HistRelBCID',
                                   title='Relativ BCID Histogram',
                                   obj=hist_rel_bcid,
                                   filters=tb.Filters(complib='blosc',
                                                      complevel=5,
                                                      fletcher32=False))
            out_file.create_carray(out_file.root,
                                   name='HistEventStatus',
                                   title='Event status Histogram',
                                   obj=hist_event_status,
                                   filters=tb.Filters(complib='blosc',
                                                      complevel=5,
                                                      fletcher32=False))
            out_file.create_carray(out_file.root,
                                   name='HistBCIDError',
                                   title='Event status Histogram',
                                   obj=hist_bcid_error,
                                   filters=tb.Filters(complib='blosc',
                                                      complevel=5,
                                                      fletcher32=False))

            if scan_id in ['threshold_scan', 'global_threshold_tuning', 'local_threshold_tuning']:
                n_injections = self.run_config['n_injections']
                hist_scurve = hist_occ.reshape((192 * 400, -1))

                if scan_id == 'threshold_scan':
                    scan_param_range = [v - self.run_config['VCAL_MED'] for v in range(self.run_config['VCAL_HIGH_start'],
                                                                                       self.run_config['VCAL_HIGH_stop'], self.run_config['VCAL_HIGH_step'])]
                    self.threshold_map, self.noise_map, self.chi2_map = au.fit_scurves_multithread(hist_scurve, scan_param_range, n_injections, optimize_fit_range=False)
                elif scan_id == 'global_threshold_tuning':
                    scan_param_range = range(self.run_config['VTH_start'], self.run_config['VTH_stop'], -1 * self.run_config['VTH_step'])
                    self.threshold_map, self.noise_map, self.chi2_map = au.fit_scurves_multithread(hist_scurve, scan_param_range, n_injections=n_injections,
                                                                                                   invert_x=True, optimize_fit_range=False)
                elif scan_id == 'local_threshold_tuning':
                    min_tdac, max_tdac, _ = au.get_tdac_range(self.run_config['start_column'], self.run_config['stop_column'])
                    scan_param_range = np.arange(min_tdac, max_tdac)
                    # FIXME: error prone differentiation for diff / lin flavour
                    if min_tdac < 0:
                        invert_x = True
                    else:
                        invert_x = False
                    self.threshold_map, self.noise_map, self.chi2_map = au.fit_scurves_multithread(hist_scurve, scan_param_range, n_injections=n_injections,
                                                                                                   invert_x=invert_x, optimize_fit_range=True)

                out_file.create_carray(out_file.root, name='ThresholdMap', title='Threshold Map', obj=self.threshold_map,
                                       filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
                out_file.create_carray(out_file.root, name='NoiseMap', title='Noise Map', obj=self.noise_map,
                                       filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))

                out_file.create_carray(out_file.root, name='Chi2Map', title='Chi2 / ndf Map', obj=self.chi2_map,
                                       filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))

    def _create_additional_cluster_data(self, hist_cs_size, hist_cs_tot, hist_cs_shape):
        '''
            Store cluster histograms in analyzed data file
        '''
        with tb.open_file(self.analyzed_data_file, 'r+') as out_file:
            out_file.create_carray(out_file.root,
                                   name='HistClusterSize',
                                   title='Cluster Size Histogram',
                                   obj=hist_cs_size,
                                   filters=tb.Filters(complib='blosc',
                                                      complevel=5,
                                                      fletcher32=False))
            out_file.create_carray(out_file.root,
                                   name='HistClusterTot',
                                   title='Cluster ToT Histogram',
                                   obj=hist_cs_tot,
                                   filters=tb.Filters(complib='blosc',
                                                      complevel=5,
                                                      fletcher32=False))
            out_file.create_carray(out_file.root,
                                   name='HistClusterShape',
                                   title='Cluster Shape Histogram',
                                   obj=hist_cs_shape,
                                   filters=tb.Filters(complib='blosc',
                                                      complevel=5,
                                                      fletcher32=False))

    def mask_disabled_pixels(self, enable_mask, run_config):
        mask = np.invert(enable_mask)

        mask[:run_config.get('start_column', 0), :] = True
        mask[run_config.get('stop_column', 400):, :] = True
        mask[:, :run_config.get('start_row', 0)] = True
        mask[:, run_config.get('stop_row', 192):] = True

        return mask


if __name__ == "__main__":
    pass
