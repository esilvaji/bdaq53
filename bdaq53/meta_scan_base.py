#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import time
import os
import logging
import yaml

import tables as tb

from rd53a import RD53A  # Used to get loglevel
from scan_base import ScanBase
from analysis import analysis_utils as au

loglevel = logging.getLogger('RD53A').getEffectiveLevel()


class MetaScanBase(ScanBase):
    '''
        Basic run meta class.
        Base class for scan- / tune- / analyze-class.
    '''

    def __init__(self, bench_config=None):
        self.configuration = au.ConfigDict()
        self.proj_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

        if bench_config is None:
            bench_config = os.path.join(self.proj_dir, 'bdaq53' + os.sep + 'testbench.yaml')

        with open(bench_config) as f:
            self.configuration['bench'] = yaml.load(f)

        if self.configuration['bench']['output_directory'] is not None:
            self.working_dir = self.configuration['bench']['output_directory']
        else:
            self.working_dir = os.path.join(os.getcwd(), "output_data")
        if not os.path.exists(self.working_dir):
            os.makedirs(self.working_dir)

        if not os.path.isfile(self.configuration['bench']['chip_configuration']):
            self.configuration['bench']['chip_configuration'] = os.path.join(self.proj_dir, 'bdaq53' + os.sep + 'scans' + os.sep + self.configuration['bench']['chip_configuration'])
        with open(self.configuration['bench']['chip_configuration'], 'r') as f:
            self.configuration['chip'] = yaml.load(f)

        self.timestamp = time.strftime("%Y%m%d_%H%M%S")
        self.run_name = self.timestamp + '_' + self.scan_id
        self.output_filename = os.path.join(self.working_dir, self.run_name)
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.setLevel(loglevel)
        self.setup_logfile()

        self.logger.info('Initializing %s...' % self.scan_id)
        self.scans = []

        self.chip = None

    def start(self, **kwargs):
        '''
            Prepares the scan and starts the actual test routine
        '''

        self.configuration['scan'] = kwargs

        # FIXME: Better usage of configuration object instead of kwargs!
        kwargs.update(self.configuration['chip'])
        kwargs.update(self.configuration['bench'])

        with tb.open_file(self.output_filename + '.h5', mode='w', title=self.scan_id) as self.h5_file:
            with tb.open_file(self.output_filename + '_interpreted.h5', mode='w', title=self.scan_id) as self.interpreted_data_file:
                self.dump_configuration(**kwargs)
                self.scan(**kwargs)
                self.clean_up()

    def clean_up(self):
        self.logger.info('Cleaning up output data...')

        self.interpreted_data_file.create_group(self.interpreted_data_file.root, name='configuration', title='Configuration')
        self.interpreted_data_file.copy_children(self.h5_file.root.configuration, self.interpreted_data_file.root.configuration, recursive=True)

        for scanfile in self.scans:
            with tb.open_file(scanfile.output_filename + '.h5', mode='r') as infile:
                group = self.h5_file.create_group(self.h5_file.root, os.path.basename(infile.filename).split('.')[0])
                infile.copy_node(infile.root, group, recursive=True)

            with tb.open_file(scanfile.output_filename + '_interpreted.h5', mode='r') as infile:
                group = self.interpreted_data_file.create_group(self.interpreted_data_file.root, os.path.basename(infile.filename).split('.')[0])
                infile.copy_node(infile.root, group, recursive=True)
            try:
                os.remove(scanfile.output_filename + '.h5')
                os.remove(scanfile.output_filename + '_interpreted.h5')
                os.remove(scanfile.output_filename + '_interpreted.pdf')
                os.remove(scanfile.output_filename + '.log')
            except OSError:
                pass

    def analyze(self):
        pass
