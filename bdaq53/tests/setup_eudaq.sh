git clone -b v1.7-dev https://github.com/eudaq/eudaq
cd eudaq/build
cmake -DBUILD_python=ON -DBUILD_gui=OFF -DBUILD_onlinemon=OFF -DBUILD_runsplitter=OFF ..
make -j 4
make install
cd ${PWD}/../python/
export PYTHONPATH="${PYTHONPATH}:${PWD}"