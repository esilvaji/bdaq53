export SIM=ius

if [ -d "unisims" ] && [ ! -d "secureip" ]; then
    echo "Directories unisims and secureip already exist"
else
    echo "Compiling unisims and secureip libs"

    mkdir unisims
    mkdir secureip

    echo 'DEFINE unisims  ./unisims' > cds.lib
    echo 'DEFINE secureip  ./secureip' >> cds.lib

    ncvlog -64bit -work unisims $XILINX_VIVADO/data/verilog/src/unisims/*.v
    ncvlog -64bit -work unisims $XILINX_VIVADO/data/verilog/src/retarget/*.v

    ncvlog -64bit -work secureip -f $XILINX_VIVADO/data/secureip/gtxe2_common/gtxe2_common_cell.list.f
    ncvlog -64bit -work secureip -f $XILINX_VIVADO/data/secureip/gtxe2_channel/gtxe2_channel_cell.list.f

    #ncvlog -64bit -work secureip -f $XILINX/secureip/in_fifo/in_fifo_cell.list.f
    #ncvlog -64bit -work secureip -f $XILINX/secureip/out_fifo/out_fifo_cell.list.f
    #ncvlog -64bit -work secureip -f $XILINX/secureip/iserdese2/iserdese2_cell.list.f
    #ncvlog -64bit -work secureip -f $XILINX/secureip/oserdese2/oserdese2_cell.list.f
    #ncvlog -64bit -work secureip -f $XILINX/secureip/phaser_in/phaser_in_cell.list.f
    #ncvlog -64bit -work secureip -f $XILINX/secureip/phaser_out/phaser_out_cell.list.f
    #ncvlog -64bit -work secureip -f $XILINX/secureip/phy_control/phy_control_cell.list.f
fi

