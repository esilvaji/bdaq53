/**
 * ------------------------------------------------------------
 * Copyright (c) All rights reserved
 * SiLab, Institute of Physics, University of Bonn
 * ------------------------------------------------------------
 */

`timescale 1ns / 1ps

//`default_nettype none

`include "cmd_rd53/cmd_rd53.v"
`include "cmd_rd53/cmd_rd53_core.v"

`include "utils/bus_to_ip.v"
`include "utils/cdc_pulse_sync.v"
`include "utils/cdc_reset_sync.v"
`include "utils/cdc_syncfifo.v"
`include "utils/generic_fifo.v"
`include "utils/clock_multiplier.v"
`include "gpio/gpio.v"

`include "bram_fifo/bram_fifo.v"
`include "bram_fifo/bram_fifo_core.v"

`include "fast_spi_rx/fast_spi_rx.v"
`include "fast_spi_rx/fast_spi_rx_core.v"

module tb (
		input wire          BUS_CLK,
		input wire          BUS_RST,
		input wire  [31:0]  BUS_ADD,
		inout wire  [31:0]  BUS_DATA,
		input wire          BUS_RD,
		input wire          BUS_WR,
		output wire         BUS_BYTE_ACCESS,
		output wire         CMD_EN,
		output wire			CMD_SERIAL_OUT
	);


	localparam CMD_RD53_BASEADDR = 32'h0000;
	localparam CMD_RD53_HIGHADDR = 32'h2000-1;

    localparam FAST_SR_AQ_BASEADDR = 32'h2000;
    localparam FAST_SR_AQ_HIGHADDR = 32'h3000-1;

    localparam FIFO_BASEADDR = 32'h8000;
    localparam FIFO_HIGHADDR = 32'h9000-1;

    localparam FIFO_BASEADDR_DATA = 32'h8000_0000;
    localparam FIFO_HIGHADDR_DATA = 32'h9000_0000;

    localparam GPIO_BASEADDR = 32'h3000;
    localparam GPIO_HIGHADDR = 32'h300f;
    wire [15:0] IO;

	localparam ABUSWIDTH = 32;
	assign BUS_BYTE_ACCESS = BUS_ADD < 32'h8000_0000 ? 1'b1 : 1'b0;


    wire CMD_CLK;
    clock_multiplier
    #( .MULTIPLIER(2)
    ) i_clock_multiplier
    (   .CLK(BUS_CLK),
        .CLOCK(CMD_CLK)
    );


    gpio
    #(
        .BASEADDR(GPIO_BASEADDR),
        .HIGHADDR(GPIO_HIGHADDR),
        .IO_WIDTH(16),
        .IO_DIRECTION(16'hffff),
        .IO_TRI(16'hffff)
	) i_gpio1
    (
        .BUS_CLK(BUS_CLK),
        .BUS_RST(BUS_RST),
        .BUS_ADD(BUS_ADD),
        .BUS_DATA(BUS_DATA),
        .BUS_RD(BUS_RD),
        .BUS_WR(BUS_WR),
        .IO(IO)
    );


	cmd_rd53
    #(
        .BASEADDR(CMD_RD53_BASEADDR),
        .HIGHADDR(CMD_RD53_HIGHADDR),
        .ABUSWIDTH(ABUSWIDTH)
    ) i_cmd_rd53
    (
        .BUS_CLK(BUS_CLK),
        .BUS_RST(BUS_RST),
        .BUS_ADD(BUS_ADD),
        .BUS_DATA(BUS_DATA[7:0]),
        .BUS_RD(BUS_RD),
        .BUS_WR(BUS_WR),

        .EXT_START_PIN(IO[15]),
        .EXT_TRIGGER(IO[14:0]),

    	.CMD_CLK(CMD_CLK),
        .CMD_EN(CMD_EN),
    	.CMD_SERIAL_OUT(CMD_SERIAL_OUT)
	);


	wire FIFO_READ_SPI_RX;
    wire FIFO_EMPTY_SPI_RX;
    wire [31:0] FIFO_DATA_SPI_RX;
//	wire SEN = 1'b1;

    fast_spi_rx
    #(
        .BASEADDR(FAST_SR_AQ_BASEADDR),
        .HIGHADDR(FAST_SR_AQ_HIGHADDR),
        .ABUSWIDTH(ABUSWIDTH)
    ) i_pixel_sr_fast_rx
    (
        .BUS_CLK(BUS_CLK),
        .BUS_RST(BUS_RST),
        .BUS_ADD(BUS_ADD),
        .BUS_DATA(BUS_DATA[7:0]),
        .BUS_RD(BUS_RD),
        .BUS_WR(BUS_WR),

        .SCLK(~CMD_CLK),
        .SDI(CMD_SERIAL_OUT),
        .SEN(CMD_EN), //SEN

        .FIFO_READ(FIFO_READ_SPI_RX),
        .FIFO_EMPTY(FIFO_EMPTY_SPI_RX),
        .FIFO_DATA(FIFO_DATA_SPI_RX)

    );



    wire FIFO_READ, FIFO_EMPTY;
    wire [31:0] FIFO_DATA;
    assign FIFO_DATA = FIFO_DATA_SPI_RX;
    assign FIFO_EMPTY = FIFO_EMPTY_SPI_RX;
    assign FIFO_READ_SPI_RX = FIFO_READ;

    bram_fifo
    #(
        .BASEADDR(FIFO_BASEADDR),
        .HIGHADDR(FIFO_HIGHADDR),
        .BASEADDR_DATA(FIFO_BASEADDR_DATA),
        .HIGHADDR_DATA(FIFO_HIGHADDR_DATA),
        .ABUSWIDTH(ABUSWIDTH)
    ) i_out_fifo (
        .BUS_CLK(BUS_CLK),
        .BUS_RST(BUS_RST),
        .BUS_ADD(BUS_ADD),
        .BUS_DATA(BUS_DATA),
        .BUS_RD(BUS_RD),
        .BUS_WR(BUS_WR),

        .FIFO_READ_NEXT_OUT(FIFO_READ),
        .FIFO_EMPTY_IN(FIFO_EMPTY),
        .FIFO_DATA(FIFO_DATA),

        .FIFO_NOT_EMPTY(),
        .FIFO_FULL(),
        .FIFO_NEAR_FULL(),
        .FIFO_READ_ERROR()
    );


	initial begin
		$dumpfile("../cmd_rd53.vcd");
		$dumpvars(0);
	end

endmodule
